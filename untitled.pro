#-------------------------------------------------
#
# Project created by QtCreator 2015-11-26T21:19:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    metric1.cpp

HEADERS  += mainwindow.h \
    metric1.h

FORMS    += mainwindow.ui
