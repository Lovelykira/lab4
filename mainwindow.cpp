#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    metric1 M;
    ui->textBrowser->append("M.AverageNumCodeStr()");
    ui->textBrowser->append(QString::number(M.AverageNumCodeStr()));

    ui->textBrowser->append("M.NumMethods()");
    ui->textBrowser->append(QString::number(M.NumMethods()));

    ui->textBrowser->append("M.NumModulsInclude()");
    ui->textBrowser->append(QString::number(M.NumModulsInclude()));

    ui->textBrowser->append("M.NumClasses()");
    ui->textBrowser->append(QString::number(M.NumClasses()));

    ui->textBrowser->append("M.AverageNumClassStr()");
    ui->textBrowser->append(QString::number(M.AverageNumClassStr()));

    ui->textBrowser->append("M.NORM()");
    ui->textBrowser->append(QString::number(M.NORM()));

    ui->textBrowser->append("M.RFC()");
    ui->textBrowser->append(QString::number(M.RFC()));

    ui->textBrowser->append("M.MWC()");
    ui->textBrowser->append(QString::number(M.MWC()));

    ui->textBrowser->append("M.NOC()");
    ui->textBrowser->append(QString::number(M.NOC()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
