#ifndef metric11_H
#define metric1_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QDebug>
#include <QTextCodec>
#include <math.h>

class metric1 : public QObject
{
    Q_OBJECT

private:
    QFile *file;
public:
    explicit metric1(QObject *parent = 0);
    float AverageNumCodeStr();
    float NumMethods();
    float NumModulsInclude();
    float NumClasses();
    float AverageNumClassStr();
    int NORM(); //количество удаленных методов класса
    int RFC(); //количество методов, которые могут быть вызваны экземпляром класса
    float MWC(); //сложность методов класса
    float NOC(); //количество классов-наследников
signals:

public slots:
};

#endif // metric1_H
